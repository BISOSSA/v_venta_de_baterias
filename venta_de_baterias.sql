CREATE OR REPLACE VIEW VENTA_DE_BATERIAS AS
WITH baterias AS
 (SELECT sm.*
        ,REPLACE('SOS ' || sp.desc_obse, '/', '') cod_de_bateria
        ,REPLACE(REPLACE(sm.nume_seri, '-', ''), ' ', '') nro_serie
        ,sp.nume_sap codigo_sap
        ,sp.desc_obse
    FROM stockmovimientos sm
    LEFT JOIN stockproductos sp
      ON sp.nume_prod = sm.nume_prod
   WHERE sm.impo_fina_cobr >= 0
     AND nume_serv IS NOT NULL
     AND sm.nume_esta = 1)

SELECT bat.nume_serv bat_nume_serv
      ,bat.fech_oper_ulti
      ,bat.nume_prod
      ,to_char(s.fech_serv, 'MM/DD/YYYY', 'NLS_DATE_LANGUAGE = SPANISH') fecha
      ,to_char(s.fech_serv, 'YYYYMM') id_mes
      ,to_char(s.fech_serv, 'MONTH YYYY', 'NLS_DATE_LANGUAGE = SPANISH') ds_mes
      ,to_char(shr.hora_pedi, 'hh24') hora_del_dia
      ,to_char(s.fech_serv, 'DAY', 'NLS_DATE_LANGUAGE = SPANISH') dia_de_semana
      ,s.nume_serv
      ,SUM(CASE
             WHEN sp.nume_prod = 123 THEN
              sp.impo_corr / 1.21
           END) adicional_venta_bateria
      ,SUM(CASE
             WHEN sp.nume_prod = 123 THEN
              sp.impo_corr
           END) adicional_venta_bateria_civa
      ,SUM(CASE
             WHEN sp2.nume_prod = 124 THEN
              sp2.impo_corr
           END) comision_venta
      ,pers.nomb_pers derivador
      ,p.nume_pres cta_cte_prestador
      ,p.nomb_pres nombre_prestador
      ,s.nume_base_fact base
      ,SYSDATE fecha_actual
      ,r.nomb_regi regionsvc
      ,NVL(l.localidades_agrupadas, ('A agrupar ' || loc.nomb_loca)) localidad
      ,NVL(l.nomb_loca, ('A agrupar ' || loc.nomb_loca)) localidad_real
      ,COUNT(CASE
               WHEN st.nume_esta_pago = 1 THEN
                s.nume_serv
             END) tarjeta
      ,(SELECT ic.nomb_cond
          FROM ivacondiciones ic
         WHERE ic.nume_cond = p.nume_cond) condicion_tributaria
      ,(SELECT CASE
                 WHEN ic.nume_cond = 1 THEN
                  1.21
                 ELSE
                  1
               END
          FROM ivacondiciones ic
         WHERE ic.nume_cond = p.nume_cond) condicion_tributaria_valor
      ,st.fecha_carg
      ,CASE
         WHEN st.nume_esta_pago = 1 THEN
          st.impo_tarj
       END importe_tarjeta_met
      ,(CASE
         WHEN sp.nume_prod = 123 THEN
          sp.impo_corr
       END + nvl((CASE
                    WHEN sp3.nume_prod = 130 THEN
                     sp3.impo_corr
                  END)
                 ,0)) - nvl(CASE
                              WHEN st.nume_esta_pago = 1 THEN
                               st.impo_tarj
                            END
                           ,0) importe_efectivo
      ,CASE
         WHEN COUNT(s.nume_serv) over(PARTITION BY s.nume_serv) = 1 THEN --> CONTROLA QUE NO SE DUPLIQUE EL SERVICIO
          CASE
            WHEN ((CASE
                   WHEN sp.nume_prod = 123 THEN
                    sp.impo_corr
                 END + nvl((CASE
                               WHEN sp3.nume_prod = 130 THEN
                                sp3.impo_corr
                             END)
                            ,0)) - SUM(CASE
                                          WHEN st.nume_esta_pago = 1 THEN
                                           st.impo_tarj
                                        END)) <> 0 THEN
             1
          END
       END tarjeta_efectivo
      ,COUNT(s.nume_serv) over(PARTITION BY s.nume_serv) flag_error
      ,COUNT(DISTINCT CASE
               WHEN st.nume_esta_pago = 1 AND s.fech_serv < to_date('13/04/2019', 'DD/MM/YYYY') THEN
                s.nume_serv
             END) tarjeta_antes13
      ,COUNT(DISTINCT CASE
               WHEN st.nume_esta_pago = 1 AND s.fech_serv >= to_date('13/04/2019', 'DD/MM/YYYY') THEN
                s.nume_serv
             END) tarjeta_despues13
      ,st.nomb_titu_tarj titular_tarjeta
      ,st.nume_docu nro_documento
      ,st.impo_tarj importe_tarjeta
      ,st.impo_cuot importe_cuota
      ,st.cant_cuot cant_cuotas
      ,st.nume_esta_fact_elec
      ,st.nomb_fact_elec
      ,st.nume_tipo_docu
      ,(SELECT td.nemo_tipo_docu
          FROM dba_sosdw.tipos_documentos td
         WHERE td.nume_tipo_docu = st.nume_tipo_docu) DNI_CUIT_CUIL
       ,COUNT(DISTINCT s.nume_serv) baterias
       ,COUNT(DISTINCT CASE
               WHEN s.fech_serv < to_date('13/04/2019', 'DD/MM/YYYY') THEN
                s.nume_serv
             END) ventas_antes13
       ,COUNT(DISTINCT CASE
               WHEN s.fech_serv >= to_date('13/04/2019', 'DD/MM/YYYY') THEN
                s.nume_serv
             END) ventas_despues13
       ,sg.nomb_diag diagnostico
       ,bat.cod_de_bateria
       ,bat.desc_obse
       ,bat.nro_serie
       ,bat.codigo_sap
       ,ss.marc_vehi vehiculo
       ,ss.mode_vehi modelo
       ,s.nume_esta_serv
       ,es.nomb_esta_serv estado_servicio
       ,CASE
         WHEN s.nume_esta_serv = 85 -- FACTURADO
          THEN
          1
         ELSE
          0
       END cant_facturado
       ,CASE
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 1 AND 7 THEN
          1
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 8 AND 14 THEN
          2
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 15 AND 21 THEN
          3
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 22 AND 28 THEN
          4
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 29 AND 31 THEN
          5
       END id_semanas_del_mes
       ,CASE
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 1 AND 7 THEN
          'SEMANA 1'
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 8 AND 14 THEN
          'SEMANA 2'
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 15 AND 21 THEN
          'SEMANA 3'
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 22 AND 28 THEN
          'SEMANA 4'
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 29 AND 31 THEN
          'SEMANA 5'
       END semanas_del_mes
       ,cr.nomb_radi radio
       ,comp.nomb_comp compania
       ,sp.nume_prod codigo_producto
       ,produ.nomb_prod producto
       ,CASE
         WHEN st.nume_tipo_tarj IS NULL THEN
          'EFECTIVO'
         ELSE
          'CREDITO'
       END tipo_de_pago
       ,st.impo_tarj importe_tarjeta_producto
       ,to_date(substr(st.nume_serv, 1, 8), 'YYYY/MM/DD') fecha_de_compra
       ,substr(st.nume_tarj, -4) numero_tarjeta
       ,decode(st.nume_tipo_tarj, 17, 'POSNET', 16, 'MERCADOPAGO', 'OTRO') tipo_tarjeta
       ,CASE
         WHEN cr.nume_radi = 27 THEN
          1
         ELSE
          0
       END recambio_baterias
       ,CASE
         WHEN sp3.nume_prod = 130 THEN
          1
         ELSE
          0
       END cantidad_cascos
  FROM baterias bat
  JOIN servicios s
    ON bat.nume_serv = s.nume_serv
  JOIN estadosservicio es
    ON es.nume_esta_serv = s.nume_esta_serv
  LEFT JOIN serviciosproductos sp
    ON sp.nume_serv = s.nume_serv
   AND sp.nume_prod = 123
   AND sp.flag_cobr = 1 --> ADICIONAL VENTA DE BATERIA
  LEFT JOIN serviciosproductos sp2
    ON sp2.nume_serv = s.nume_serv
   AND sp2.nume_prod = 124 --> COMISION VENTA
  LEFT JOIN serviciosproductos sp3
    ON sp3.nume_serv = s.nume_serv
   AND sp3.nume_prod = 130
   AND sp3.flag_cobr = 1 -->ADICIONAL CASCO NO ENTREGADO
  LEFT JOIN servicioshorariosreales shr
    ON shr.nume_serv = s.nume_serv
  LEFT JOIN bases b
    ON b.nume_base = s.nume_base_fact
  LEFT JOIN prestadores p
    ON p.nume_pres = b.nume_pres
  LEFT JOIN serviciosdirecciones sd
    ON sd.nume_serv = s.nume_serv
   AND sd.nume_tipo_dire = 1
  LEFT JOIN serviciossocios ss
    ON ss.nume_serv = s.nume_serv
  LEFT JOIN companiasradios cr
    ON cr.nume_radi = ss.nume_radi
  LEFT JOIN companias comp
    ON comp.nume_comp = cr.nume_comp
  LEFT JOIN localidades_agrupadas l
    ON l.nume_loca = sd.nume_loca
  LEFT JOIN dba_sos.zonas zon
    ON zon.nume_zona = l.nume_zona --> PARA IDENTIFICAR LOS DIFERENTES BARRIOS DE LA PLATA
  LEFT JOIN provincias prov
    ON prov.nume_prov = l.nume_prov
  LEFT JOIN LOCALIDADES LOC
    ON LOC.NUME_LOCA = SD.NUME_LOCA
  LEFT JOIN regiones r
    ON r.nume_regi = l.nume_regi
  LEFT JOIN personal pers
    ON pers.nume_pers = s.nume_deri
  LEFT JOIN serviciostarjetas st
    ON st.nume_serv = s.nume_serv
   AND st.nume_esta_pago = 1
  LEFT JOIN productos produ
    ON sp.nume_prod = produ.nume_prod

  LEFT JOIN dba_sos.diagnosticos sg
    ON sg.nume_diag = s.nume_diag_esti
 WHERE s.nume_cier >= 100
    OR (SELECT SUM(sp.impo_corr)
          FROM serviciosproductos sp
          JOIN productos prod
            ON prod.nume_prod = sp.nume_prod
          JOIN productoscategorias pc
            ON pc.nume_cate = prod.nume_cate
           AND pc.clas_cate = 'P'
         WHERE sp.nume_serv = s.nume_serv) > 0
 GROUP BY bat.nume_serv
         ,bat.fech_oper_ulti
         ,bat.nume_prod
         ,s.nume_serv
         ,s.fech_serv
         ,shr.hora_pedi
         ,pers.nomb_pers
         ,p.nume_pres
         ,p.nomb_pres
         ,s.nume_base_fact
         ,r.nomb_regi
         ,st.nume_esta_pago
         ,st.nume_plan
         ,sg.nomb_diag
         ,l.nume_regi
         ,l.nume_loca
         ,l.nomb_loca
         ,zon.nume_macr_zona
         ,bat.cod_de_bateria
         ,bat.desc_obse
         ,ss.marc_vehi
         ,ss.mode_vehi
         ,s.nume_esta_serv
         ,es.nomb_esta_serv
         ,l.nume_prov
         ,prov.nomb_prov
         ,st.nomb_titu_tarj
         ,st.nume_docu
         ,st.impo_tarj
         ,st.impo_cuot
         ,st.cant_cuot
         ,st.nume_esta_fact_elec
         ,st.nomb_fact_elec
         ,st.nume_tipo_docu
         ,cr.nomb_radi
         ,comp.nomb_comp
         ,bat.nro_serie
         ,bat.codigo_sap
         ,sp.nume_prod
         ,sp.impo_corr
         ,st.fecha_carg
         ,p.nume_cond
         ,sp3.nume_prod
         ,sp3.impo_corr
         ,produ.nomb_prod
         ,st.nume_tipo_tarj
         ,st.nume_serv
         ,loc.nomb_loca
         ,st.nume_tarj
         ,cr.nume_radi
         ,sp3.nume_prod
         ,l.localidades_agrupadas
;
